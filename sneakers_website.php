<?php
//https://www.webslesson.info/2016/04/append-data-to-json-file-using-php.html
$message = '';
$error = '';
if (isset($_POST["submit"]))
{
    //display an error message at the top of the form if one of the fields is empty
    if (empty($_POST["name"]))
    {
        $error = "<label class='text-danger'>Enter a Name</label>";
    }
    else if (empty($_POST["description"]))
    {
        $error = "<label class='text-danger'>Enter a Description</label>";
    }
    else if (empty($_POST["brand"]))
    {
        $error = "<label class='text-danger'>Enter a Brand</label>";
    }
    else if (empty($_POST["sizes"]))
    {
        $error = "<label class='text-danger'>Enter a Size</label>";
    }
    else if (empty($_POST["localization"]))
    {
        $error = "<label class='text-danger'>Enter a Place</label>";
    }
    else if (!file_exists($_FILES['pictures']['tmp_name'][0]) || !is_uploaded_file($_FILES['pictures']['tmp_name'][0]))
    {
        $error = "<label class='text-danger'>Enter an Image</label>";
    }
    else
    {
        if (file_exists('sneakers.json'))
        {
            $current_data = file_get_contents('sneakers.json');
            $array_data = json_decode($current_data, true);
            //create a new array to append to the JSON file
            $extra = array(
                'name' => $_POST['name'],
                'description' => $_POST["description"],
                'brand' => $_POST["brand"],
                'sizes' => $_POST['sizes'],
                'pictures' => array(
                    $_FILES['pictures']['name'][0]
                ) ,
                'localization' => $_POST["localization"]
            );
            $array_data[] = $extra;
            $final_data = json_encode($array_data);
            //success message displayed at the bottom of the form if the form was filled correctly
            if (file_put_contents('sneakers.json', $final_data))
            {
                $message = "<label class='text-success'>Sneaker added successfully</p>";
            }

        }
        else
        {
            $error = 'JSON File does not exist';
        }

        //move the selected image to the /images directory so it can be displayed in the table
        $name = $_FILES['pictures']['name'][0];
        $temp_name = $_FILES['pictures']['tmp_name'][0];
        if (isset($name) and !empty($name))
        {
            $location = 'images/';
            if (move_uploaded_file($temp_name, $location . $name))
            {
                echo 'File uploaded successfully';
            }

        }
    }
}

//https://stackoverflow.com/questions/18213026/delete-element-from-json-with-php/48871710
if (isset($_POST["delete"]))
{

    $data = file_get_contents('sneakers.json');
    $json_arr = json_decode($data, true);
    $arr_index = array();
    foreach ($json_arr as $key => $value)
    {
        if ($value['name'] == $_POST["name"])
        {
            $arr_index[] = $key;
        }
    }

    //delete the sneaker object
    foreach ($arr_index as $i)
    {
        unset($json_arr[$i]);
    }

    $json_arr = array_values($json_arr);

    file_put_contents('sneakers.json', json_encode($json_arr));
}

if (isset($_POST["save"]))
{

    $jsonString = file_get_contents('sneakers.json');
    $data = json_decode($jsonString, true);

    //get values from the table and set it to the keys
    $data[$_POST["counter"]]['name'] = $_POST["name"];
    $data[$_POST["counter"]]['description'] = $_POST["description"];
    $data[$_POST["counter"]]['brand'] = $_POST["brand"];
    $data[$_POST["counter"]]['sizes'] = $_POST["sizes"];

    if (isset($_FILES["pictures"]["name"]))
    {

        $name = $_FILES["pictures"]["name"];
        $tmp_name = $_FILES['pictures']['tmp_name'];

        //if an image has been uploaded, add the image to the /images directory
        if (!empty($name))
        {
            $location = 'images/';
            array_push($data[$_POST["counter"]]['pictures'], $_FILES["pictures"]["name"]);
            if (move_uploaded_file($tmp_name, $location . $name))
            {
                echo 'File uploaded successfully';
            }

        }

        $data[$_POST["counter"]]['localization'] = $_POST["localization"];

        $newJsonString = json_encode($data);
        file_put_contents('sneakers.json', $newJsonString);
    }
}
?>


<!DOCTYPE html>
<html>

<head>
    <title>Sneaker Store</title>
    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.6/css/bootstrap.min.css" />
    <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.5.1/jquery.min.js"></script>
    <link rel="stylesheet" href="styles.css">
    <script type="text/javascript" src="home.js"></script>
    <script type="text/javascript" src="tabs.js"></script>
    <script type="text/javascript" src="sneakers.js"></script>
    
</head>

<body>

    <div class="tab">
        <button class="tablinks" onclick="showTab(event, 'Add')">Add a Sneaker</button>
        <button class="tablinks" onclick="showTab(event, 'List')">List</button>
    </div>

    <div id="Add" class="tabcontent">
        <div class="container" style="width:500px;">
            <h2>Add a Sneaker</h2><br />
            <form id="addSneaker" enctype="multipart/form-data" method="post">
                <?php
                    if (isset($error))
                    {
                        echo $error;
                    }
                ?>
                <br />
                <label>Name</label>
                <input type="text" name="name" class="form-control" /><br />
                <label>Description</label>
                <input type="text" name="description" class="form-control" /><br />
                <label>Brand</label>
                <select name="brand">
                    <option value="Nike">Nike</option>
                    <option value="Adidas">Adidas</option>
                </select><br />
                <label>Sizes</label>
                <div class="checkboxes">
                    <label for="5"><input type="checkbox" id="5" name="sizes[]" value="5" /> <span>5</span></label>
                    <label for="6"><input type="checkbox" id="6" name="sizes[]" value="6" /> <span>6</span></label>
                    <label for="7"><input type="checkbox" id="7" name="sizes[]" value="7" /> <span>7</span></label>
                    <label for="8"><input type="checkbox" id="8" name="sizes[]" value="8" /> <span>8</span></label>
                    <label for="9"><input type="checkbox" id="9" name="sizes[]" value="9" /> <span>9</span></label>
                    <label for="10"><input type="checkbox" id="10" name="sizes[]" value="10" /> <span>10</span></label>
                    <label for="11"><input type="checkbox" id="11" name="sizes[]" value="11" /> <span>11</span></label>
                    <label for="12"><input type="checkbox" id="12" name="sizes[]" value="12" /> <span>12</span></label>
                </div>
                <label>Localization</label>
                <select name="localization">
                    <option value="Montréal">Montréal</option>
                    <option value="Québec">Québec</option>
                    <option value="Sherbrooke">Sherbrooke</option>
                </select><br />
                <label>Picture</label>
                <input type="file" name="pictures[]" class="form-control" />
                <input type="hidden" name="pictures[]"/>
                
                <div class="buttonHolder">
                    <input class="hover-center-2" type="submit" name="submit" value="Add">
                </div>

                <?php
                    if (isset($message))
                    {
                        echo $message;
                    }
                 ?>

            </form>

        </div>
    </div>

    <div id="List" class="tabcontent">
        <h2>Sneakers</h2>
        <table>
            <tr>
                <th>Picture</th>
                <th>Name</th>
                <th>Description</th>
                <th>Brand</th>
                <th>Sizes</th>
                <th>Localization</th>
            </tr>

            <?php
                $data = file_get_contents("sneakers.json");
                $data = json_decode($data, true);
                $counter = 0; //we need a counter to know which sneaker object has been edited
                foreach ($data as $row)
                {
                    echo '<form enctype="multipart/form-data" method="post" class="action" > ';
                    echo '<tr><td><div class="pic-container">';
                    //display the picture(s)
                    foreach ($row["pictures"] as $pic)
                    {
                        echo '<img src="images/' . $pic . '">';
                    }
                    echo '</div><input id="upload" accept="image/*" type="file" name="pictures"/></td>';
                    echo '<td><input type="text" name="name" value="' . $row["name"] . '"/></td>';
                    echo '<td><textarea name="description" value="' . $row["description"] . '"/>' . $row["description"] . '</textarea></td>';

                    echo '<td><select name="brand"">';
                    $brands = ['Nike', 'Adidas'];
                    //select and create the option based on the object in the JSON file for the brand
                    foreach ($brands as $value)
                    {
                        if ($value == $row["brand"])
                        {
                            echo '<option value="' . $row["brand"] . '" selected="selected">' . $row["brand"] . '</option>';
                        }
                        else
                        {
                            echo '<option value="' . $value . '">' . $value . '</option>';
                        }
                    }
                    echo '</select></td>';
                    //create the checkboxes for the sizes and select/check the ones based on the object in the JSON file
                    echo '<td><div class="checkboxes">';
                    for ($x = 5;$x <= 12;$x++)
                    {
                        if (in_array($x, $row["sizes"]))
                        {
                            echo '<label for="' . $x . '"><input type="checkbox" id="' . $x . '" name="sizes[]" value="' . $x . '" checked/> <span>' . $x . '</span></label>';
                        }
                        else
                        {

                            echo '<label for="' . $x . '"><input type="checkbox" id="' . $x . '" name="sizes[]" value="' . $x . '"/> <span>' . $x . '</span></label>';
                        }
                    }
                    echo '</div></td>';

                    echo '<td><select name="localization">';
                    $places = ['Montréal', 'Québec', 'Sherbrooke'];
                    //like the brand, create and select the option based on the object in the JSON file
                    foreach ($places as $value)
                    {
                        if ($value == $row["localization"])
                        {
                            echo '<option value="' . $row["localization"] . '" selected="selected">' . $row["localization"] . '</option>';
                        }
                        else
                        {
                            echo '<option value="' . $value . '">' . $value . '</option>';
                        }
                    }
                    echo '</select></td>';
                    echo '<td><input type="submit" name="delete" value="Delete" /></td>';
                    echo '<td><input type="submit" name="save" value="Save"/></td>';
                    //hidden input to hold a counter to be used when a POST is requested for a save (to know which object has been modified)
                    echo '<td><input type="hidden" name="counter" value="' . $counter . '"></td></tr>';
                    $counter++;
                    echo '</form>';
                }

                ?>

        </table>
    </div>

</body>

</html>

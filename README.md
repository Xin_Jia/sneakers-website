**How to run the project**
- Since the code must run in a server (.php), WAMP server needs to be installed. If it has not been installed yet, you can download it here: https://www.wampserver.com/en/
- After the install, navigate to the C:/wamp64/www directory.
- Create a new empty directory and paste all the files in it.
- Start the WAMP server, go to http://localhost/yourfolder/ and click on the .php file. You can now explore the website!

**How the website works**

The Form Tab
- In order to add a new sneaker to the JSON file, every field must be filled. If one of the fields is empty, an error message will be shown at the top of the form. 
- If you upload a file that is not an image, no image will be shown in the table/list tab. 
- If you click on the Brand or Localization fields, you can choose an option. You can also choose more than one size for the sneaker. 
- When a sneaker has been successfully added, it will be shown in the next tab where every sneakers are displayed, which also means it has been added to the JSON file.

The Table/List Tab
- Every field in this tab is editable. Once you finish editing a particular sneaker, you can click on save and it will automatically be saved in the JSON file. However, I was not able to implement ajax (because it is also my first time writing php code) so whenever you click on delete or save, it will reload the page and redirect you to the first tab.
- I only added more than one picture for the first sneaker, so if you scroll with your mouse on the picture, the second picture will be shown. You can add more pictures to any sneaker by clicking on the Choose File, but you cannot remove a picture.
- A sneaker object can be removed by clicking on the Delete button, which means it will be removed from the JSON file.
